const productImageList = document.querySelectorAll('.product-card__img');
const productDescriptionList = document.querySelectorAll('.product-card__description');
const closeBtnList = document.querySelectorAll('.btn-close');
let timere = null;

productImageList.forEach((product)=>{
  product.addEventListener('mouseenter', (event)=>{
    timere = setTimeout(()=>{
      openDescription(event.target.dataset.cardId)
    }, 700)//время задержки в мил.сек
  })
})
productImageList.forEach((product)=>{
  product.addEventListener('mouseleave', (event)=>{
    clearTimeout(timere)
  })
})

closeBtnList.forEach((btn)=>{
  btn.addEventListener('click', (event)=>{
    closeDescription(event.target.dataset.cardId)
  })
})

function openDescription(data) {
  productDescriptionList.forEach(description => {
    if(description.dataset.cardId == data){
      description.classList.toggle('product-card__description--active')
    }
  })
}

function closeDescription(data) {
  productDescriptionList.forEach(description => {
    if(description.dataset.cardId == data){
      description.classList.remove('product-card__description--active')
    }
  })
}

/****************************** */
const sectionObserved = document.querySelector('.section-products');
const options = {
  root: null,
  rootMargin: '0px',
  threshold: 0.5, //область видимости принимает значения от 0 до 1
}

const observer = new IntersectionObserver(callback, options);

function callback(entries) {
  entries.forEach((element) => {
    if(element.isIntersecting){
        element.target.classList.add('section-products--active')
        observer.unobserve(element.target)
      }  
  })
}

observer.observe(sectionObserved)
